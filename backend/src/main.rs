use backend::api::router::router;
use dotenv::dotenv;
use std::env;

fn main() {
    dotenv().ok();
    env_logger::init();

    let host = env::var("HOST").expect("HOST must be set");
    let port = env::var("PORT").expect("PORT must be set");
    let address = format!("{}:{}", host, port);
    gotham::start(address, router())
}

#[cfg(test)]
mod tests {
    use super::*;
    use gotham::hyper::StatusCode;
    use gotham::test::TestServer;

    fn test_server() -> TestServer {
        TestServer::new(router()).expect("Unable to start test server")
    }

    #[test]
    fn clippy() {
        let test_server = test_server();
        let response = test_server
            .client()
            .post("http://localhost:3000/clippy", "data", mime::TEXT_PLAIN)
            .perform()
            .expect("Unable to send a request");

        assert_eq!(response.status(), StatusCode::OK);
        let body = response.read_body().expect("Unable to get response body");
        assert_eq!(body, b"[]");
    }
}
