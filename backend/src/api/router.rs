use crate::api::handlers::handle_clippy::handle_clippy;
use gotham::middleware::logger::RequestLogger;
use gotham::middleware::security::SecurityMiddleware;
use gotham::pipeline::new_pipeline;
use gotham::pipeline::single::single_pipeline;
use gotham::router::builder::{build_router, DefineSingleRoute, DrawRoutes};
use gotham::router::Router;
use log::Level;
use std::env;
use std::str::FromStr;

pub fn router() -> Router {
    let rust_log = env::var("RUST_LOG").unwrap_or_else(|_| String::from("error"));
    let log_level = Level::from_str(&rust_log).unwrap_or_else(|_| Level::Error);
    let (chain, pipelines) = single_pipeline(
        new_pipeline()
            .add(RequestLogger::new(log_level))
            .add(SecurityMiddleware)
            .build(),
    );
    build_router(chain, pipelines, |route| {
        route.post("/clippy").to_async_borrowing(handle_clippy);
    })
}
